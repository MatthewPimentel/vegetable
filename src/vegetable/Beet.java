/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

/**
 *
 * @author Matt
 */
public class Beet extends Vegetable{
    

    public Beet(String color, double size) {
        super(color, size);
        
    }
    
    @Override
    public String isRipe(){
        if(super.getColor().equals("red") && super.getSize() == 2){
            return "Ripe";
        } else{
            return "Not ripe";
        }
    }
    

    
    
    
    
    
}
