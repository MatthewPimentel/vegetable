package vegetable;

public class VegetableFactory {
    
    private static VegetableFactory vegFactory;
    
    private VegetableFactory(){
        
    }
    
    public static VegetableFactory getInstance(){
        if(vegFactory == null){
            vegFactory = new VegetableFactory();
        }
        return vegFactory;
    }
    
    public Vegetable getVegetable(VegetableTypes type, String color, double size){
        Vegetable veggie = null;
        switch (type){
            case CARROT:
                veggie = new Carrot(color,size);
                break;
            case BEET:
                veggie = new Beet(color,size);
                break;
        }
        return veggie;
    }
}
