package vegetable;

public class VegetableSimulation {
    public static void main(String [] args){
        VegetableFactory factory = VegetableFactory.getInstance();
        Vegetable  carrot = factory.getVegetable(VegetableTypes.CARROT, "orange", 2);
        Vegetable ripeCarrot = factory.getVegetable(VegetableTypes.CARROT, "orange", 1.5);
        Vegetable beet = factory.getVegetable(VegetableTypes.BEET, "yellow", 2);
        Vegetable ripeBeet = factory.getVegetable(VegetableTypes.BEET, "red", 2);
        
        System.out.println("Carrot is ripe? : " + carrot.isRipe());
        System.out.println("Carrot is ripe? : " + ripeCarrot.isRipe());
        
        System.out.println("Beet is ripe? : " + beet.isRipe());
        System.out.println("Beet is ripe? : " + ripeBeet.isRipe());
        
    }
}
